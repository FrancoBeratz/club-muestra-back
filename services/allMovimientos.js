const helper = require("../helper")
const config = require("../config")
const db = require("./db")

async function getMovimientosEgresos(page = 1) {
  const offset = helper.getOffset(page, config.listPerPage)
  const rows = await db.query(
    `SELECT id_mov as id, id_mov,titulo_mov,monto_mov,DATE_FORMAT(fecha_mov, '%d/%m/%Y') fecha_mov,motivo_mov,operacion_mov FROM movimientos WHERE operacion_mov="Egreso"`,
    [offset, config.listPerPage]
  )
  const data = helper.emptyOrRows(rows)
  const meta = { page }
  return {
    data,
    meta,
  }
}

async function getMovimientosIngresos(page = 1) {
  const offset = helper.getOffset(page, config.listPerPage)
  const rows = await db.query(
    `SELECT id_mov as id, id_mov,titulo_mov,monto_mov,DATE_FORMAT(fecha_mov, '%d/%m/%Y') fecha_mov,motivo_mov,operacion_mov FROM movimientos WHERE operacion_mov="Ingreso"`,
    [offset, config.listPerPage]
  )
  const data = helper.emptyOrRows(rows)
  const meta = { page }
  return {
    data,
    meta,
  }
}

async function removeMovimiento(id) {
  const result = await db.query(`DELETE FROM movimientos WHERE id_mov=?`, [id])
  let message = "Error, intente nuevamente."
  if (result.affectedRows) {
    message = "Movimiento eliminado correctamente"
  }
  return { message }
}

async function createMovimiento(movimiento) {
  console.log("movimientos create ", movimiento)
  const result = await db.query(
    `INSERT INTO movimientos
    (titulo_mov, monto_mov, fecha_mov, motivo_mov,operacion_mov)
    VALUES
    (?, ?, ?, ?, ?)`,
    [
      movimiento.title,
      movimiento.cant,
      movimiento.date,
      movimiento.motivo,
      movimiento.estado,
    ]
  )
  console.log("result", result)
  let message = "Error in creating programming language"
  if (result.affectedRows) {
    message = "1"
  }
  return { result, message }
}

async function updateMovimiento(id, movimiento) {
  console.log("id", id)
  console.log("movimiento", movimiento)
  const result = await db.query(
    `UPDATE movimientos
    SET titulo_mov=?, monto_mov=?, fecha_mov=?,
    motivo_mov=? WHERE id_mov=?`,
    [movimiento.title, movimiento.cant, movimiento.date, movimiento.motivo, id]
  )
  let message = "Error al actualizar este movimiento"
  console.log(result)
  if (result.affectedRows) {
    message = "1"
  }
  return { result, message }
}

async function searchMovimientoFilter(page = 1, movimientos) {
  const offset = helper.getOffset(page, config.listPerPage)
  const rows = await db.query(
    `SELECT id_mov as id, id_mov,titulo_mov,monto_mov,DATE_FORMAT(fecha_mov, '%d/%m/%Y') fecha_mov,motivo_mov,operacion_mov,(select (SUM(monto_mov)) as total_ingreso FROM movimientos where operacion_mov="Ingreso" and fecha_mov>="${movimientos.fechaDesde}" and fecha_mov<="${movimientos.fechaHasta}") as total_ingreso FROM movimientos where fecha_mov>="${movimientos.fechaDesde}" and fecha_mov<="${movimientos.fechaHasta}" and operacion_mov="Ingreso"`,
    [offset, config.listPerPage]
  )
  const egreso = await db.query(
    `SELECT id_mov as id, id_mov,titulo_mov,monto_mov,DATE_FORMAT(fecha_mov, '%d/%m/%Y') fecha_mov,motivo_mov,operacion_mov FROM movimientos where fecha_mov>="${movimientos.fechaDesde}" and fecha_mov<="${movimientos.fechaHasta}" and operacion_mov="Egreso"`,
    [offset, config.listPerPage]
  )
  const totalingresos = await db.query(
    `select SUM(monto_mov) as total_ingreso FROM movimientos where operacion_mov="Ingreso" and fecha_mov>="${movimientos.fechaDesde}" and fecha_mov<="${movimientos.fechaHasta}"`,
    [offset, config.listPerPage]
  )
  const totalegreso = await db.query(
    `select SUM(monto_mov) as total_egreso FROM movimientos where operacion_mov="Egreso" and fecha_mov>="${movimientos.fechaDesde}" and fecha_mov<="${movimientos.fechaHasta}"`,
    [offset, config.listPerPage]
  )
  const totalIngresos = helper.emptyOrRows(totalingresos)
  const totalEgreso = helper.emptyOrRows(totalegreso)
  const data = helper.emptyOrRows(rows)
  const dataEgreso = helper.emptyOrRows(egreso)

  const meta = { page }
  return {
    data,
    dataEgreso,
    totalIngresos,
    totalEgreso,
    meta,
  }
}

async function addSumaIngreso(page = 1, movimientos) {
  const offset = helper.getOffset(page, config.listPerPage)
  const ingresos = await db.query(
    `select SUM(monto_mov) as total_ingreso FROM movimientos where operacion_mov="Ingreso" and fecha_mov>="${movimientos.fechaDesde}" and fecha_mov<="${movimientos.fechaHasta}"`,
    [offset, config.listPerPage]
  )
  const egreso = await db.query(
    `select SUM(monto_mov) as total_egreso FROM movimientos where operacion_mov="Egreso" and fecha_mov>="${movimientos.fechaDesde}" and fecha_mov<="${movimientos.fechaHasta}"`,
    [offset, config.listPerPage]
  )
  const totalIngresos = helper.emptyOrRows(ingresos)
  const totalEgreso = helper.emptyOrRows(egreso)

  const meta = { page }
  return {
    totalIngresos,
    totalEgreso,
    meta,
  }
}

module.exports = {
  getMovimientosEgresos,
  getMovimientosIngresos,
  createMovimiento,
  updateMovimiento,
  removeMovimiento,
  searchMovimientoFilter,
  addSumaIngreso,
}
