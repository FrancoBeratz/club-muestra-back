const helper = require("../helper")
const config = require("../config")
const db = require("./db")

async function getMembers(page = 1) {
  const offset = helper.getOffset(page, config.listPerPage)
  const rows = await db.query(
    `SELECT  id_soc,nombre_soc,dni_soc,DATE_FORMAT(fechaNac_soc, '%d/%m/%Y') fechaNac_soc,DATE_FORMAT(fechaAfil_soc, '%d/%m/%Y') fechaAfil_soc,DATE_FORMAT(cuotaHasta_soc, '%d/%m/%Y') cuotaHasta_soc FROM socios`,
    [offset, config.listPerPage]
  )
  const data = helper.emptyOrRows(rows)
  const meta = { page }
  return {
    data,
    meta,
  }
}

async function getMembersFilterPagoOk(page = 1, member) {
  const offset = helper.getOffset(page, config.listPerPage)
  const rows = await db.query(
    `SELECT  id_soc,nombre_soc,dni_soc,DATE_FORMAT(fechaNac_soc, '%d/%m/%Y') fechaNac_soc,DATE_FORMAT(fechaAfil_soc, '%d/%m/%Y') fechaAfil_soc,DATE_FORMAT(cuotaHasta_soc, '%d/%m/%Y') cuotaHasta_soc FROM socios WHERE cuotaHasta_soc>=?`,
    [member.fechaHasta],
    [offset, config.listPerPage]
  )
  const data = helper.emptyOrRows(rows)
  const meta = { page }
  return {
    data,
    meta,
  }
}

async function getMembersFilterPagoError(page = 1, member) {
  const offset = helper.getOffset(page, config.listPerPage)
  const rows = await db.query(
    `SELECT  id_soc,nombre_soc,dni_soc,DATE_FORMAT(fechaNac_soc, '%d/%m/%Y') fechaNac_soc,DATE_FORMAT(fechaAfil_soc, '%d/%m/%Y') fechaAfil_soc,DATE_FORMAT(cuotaHasta_soc, '%d/%m/%Y') cuotaHasta_soc FROM socios WHERE cuotaHasta_soc<=?`,
    [member.fechaHasta],
    [offset, config.listPerPage]
  )
  const data = helper.emptyOrRows(rows)
  const meta = { page }
  return {
    data,
    meta,
  }
}

async function removeMember(id) {
  const result = await db.query(`DELETE FROM socios WHERE id_soc=?`, [id])
  let message = "Error in deleting programming language"
  if (result.affectedRows) {
    message = "Socio eliminado correctamente"
  }
  return { message }
}

async function createMember(member) {
  const result = await db.query(
    `INSERT INTO socios
    (nombre_soc, dni_soc, fechaNac_soc, fechaAfil_soc)
    VALUES
    (?, ?, ?, ?)`,
    [member.nameLastName, member.dni, member.fechaNac, member.fechaAfiliacion]
  )
  console.log("result", result)
  let message = "Error in creating programming language"
  if (result.affectedRows) {
    message = "1"
  }
  return { result, message }
}

async function updateMember(id, member) {
  const result = await db.query(
    `UPDATE socios
    SET nombre_soc=?, dni_soc=?, fechaNac_soc=?,
    fechaAfil_soc=? WHERE id_soc=?`,
    [
      member.nameLastName,
      member.dni,
      member.fechaNac,
      member.fechaAfiliacion,
      id,
    ]
  )
  let message = "Error in updating programming language"
  if (result.affectedRows) {
    message = "1"
  }
  return { result, message }
}

async function updateMemberPayment(id, member) {
  const result = await db.query(
    `UPDATE socios
    SET cuotaHasta_soc=? WHERE id_soc=?`,
    [member.fechaFin, id]
  )
  let message = "Error in updating programming language"
  if (result.affectedRows) {
    message = "1"
  }
  return { result, message }
}

module.exports = {
  getMembers,
  getMembersFilterPagoOk,
  getMembersFilterPagoError,
  createMember,
  updateMember,
  updateMemberPayment,
  removeMember,
}
