const helper = require("../helper")
const config = require("../config")
const db = require("./db")

async function getPlayers(page = 1, filter) {
  console.log("filter ", filter)
  if (filter == "all") {
    filter = ""
  }
  const offset = helper.getOffset(page, config.listPerPage)
  const rows = await db.query(
    `SELECT id_player,name_player,dni_player,tel_player, DATE_FORMAT(nac_player, '%d/%m/%Y') nac_player,DATE_FORMAT(alta_player, '%d/%m/%Y') alta_player,DATE_FORMAT(baja_player, '%d/%m/%Y') baja_player,origen_player,destino_player,division_player FROM jugadores WHERE division_player LIKE "%${filter}%"`,
    [offset, config.listPerPage]
  )
  const data = helper.emptyOrRows(rows)
  const meta = { page }
  return {
    data,
    meta,
  }
}

async function remove(id) {
  const result = await db.query(`DELETE FROM jugadores WHERE id_player=?`, [id])
  let message = "Error in deleting programming language"
  if (result.affectedRows) {
    message = "Jugador eliminado correctamente"
  }
  return { message }
}

async function create(player) {
  console.log("player create ", player)
  const result = await db.query(
    `INSERT INTO jugadores
    (name_player, dni_player, tel_player, nac_player, alta_player, baja_player, origen_player, destino_player,division_player)
    VALUES
    (?, ?, ?, ?, ?, ?, ?, ?, ?)`,
    [
      player.nameAlta,
      player.dniAlta,
      player.telAlta,
      player.fechaNacAlta,
      player.fechaAlta,
      player.fechaBaja,
      player.clubOrigenAlta,
      player.clubDestinoAlta,
      player.division,
    ]
  )
  console.log("result", result)
  let message = "Error in creating programming language"
  if (result.affectedRows) {
    message = "1"
  }
  return { result, message }
}

async function update(id, player) {
  const result = await db.query(
    `UPDATE jugadores
    SET name_player=?, dni_player=?, tel_player=?,
    nac_player=?, alta_player=?, baja_player=?, origen_player=?, destino_player=?, division_player=?
    WHERE id_player=?`,
    [
      player.nameAlta,
      player.dniAlta,
      player.telAlta,
      player.fechaNacAlta,
      player.fechaAlta,
      player.fechaBaja,
      player.clubOrigenAlta,
      player.clubDestinoAlta,
      player.division,
      id,
    ]
  )
  let message = "Error in updating programming language"
  console.log(result)
  if (result.affectedRows) {
    message = "1"
  }
  return { result, message }
}

module.exports = {
  getPlayers,
  create,
  update,
  remove,
}
