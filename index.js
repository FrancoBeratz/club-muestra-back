const express = require("express")
const bodyParser = require("body-parser")
const cors = require("cors")
const app = express()
const port = process.env.PORT || 3001
const allPlayers = require("./routes/players")
const allMembers = require("./routes/members")
const allMovimientos = require("./routes/movimientos")

app.use(cors())

app.get("/", (req, res) => {
  res.json({ message: "ok" })
})

app.use("/allPlayers", allPlayers)
app.use("/members", allMembers)
app.use("/movimientos", allMovimientos)

/* Error handler middleware */
app.use((err, req, res, next) => {
  const statusCode = err.statusCode || 500
  console.error(err.message, err.stack)
  res.status(statusCode).json({ message: err.message })

  return
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
