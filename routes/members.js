const express = require("express")
const router = express()
const allMembers = require("../services/allMembers")
const parser = require("body-parser")
router.use(parser.json())

router.get("/", async function (req, res, next) {
  try {
    console.log("boodddd", req.body)
    res.json(await allMembers.getMembers(req.query.page))
  } catch (err) {
    console.error(`Error while getting programming languages `, err.message)
    next(err)
  }
})

router.post("/membersStateOk", async function (req, res, next) {
  console.log("stateOk", req)
  try {
    res.json(await allMembers.getMembersFilterPagoOk(req.query.page, req.body))
  } catch (err) {
    console.error(`Error while getting programming languages `, err.message)
    next(err)
  }
})

router.post("/membersStateError", async function (req, res, next) {
  console.log("stateError", req.body)
  try {
    console.log(req)
    res.json(
      await allMembers.getMembersFilterPagoError(req.query.page, req.body)
    )
  } catch (err) {
    console.error(`Error while getting programming languages `, err.message)
    next(err)
  }
})

router.delete("/:id", async function (req, res, next) {
  try {
    res.json(await allMembers.removeMember(req.params.id))
  } catch (err) {
    console.error(`Error while deleting programming language`, err.message)
    next(err)
  }
})

router.post("/", async function (req, res, next) {
  try {
    res.json(await allMembers.createMember(req.body))
  } catch (err) {
    console.error(`Error while creating programming language`, err.message)
    next(err)
  }
})

router.put("/:id", async function (req, res, next) {
  console.log("body", req.body)
  try {
    res.json(await allMembers.updateMember(req.params.id, req.body))
  } catch (err) {
    console.error(`Error while updating programming language`, err.message)
    next(err)
  }
})

router.put("/update_payment/:id", async function (req, res, next) {
  console.log("body", req.body)
  try {
    res.json(await allMembers.updateMemberPayment(req.params.id, req.body))
  } catch (err) {
    console.error(`Error while updating programming language`, err.message)
    next(err)
  }
})

module.exports = router
