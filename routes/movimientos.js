const express = require("express")
const router = express()
const allMovimientos = require("../services/allMovimientos")
const parser = require("body-parser")
router.use(parser.json())

router.get("/egresos", async function (req, res, next) {
  try {
    res.json(await allMovimientos.getMovimientosEgresos(req.query.page))
  } catch (err) {
    console.error(`Error while getting programming languages `, err.message)
    next(err)
  }
})

router.get("/ingresos", async function (req, res, next) {
  try {
    console.log(req)
    res.json(await allMovimientos.getMovimientosIngresos(req.query.page))
  } catch (err) {
    console.error(`Error while getting programming languages `, err.message)
    next(err)
  }
})

router.delete("/:id", async function (req, res, next) {
  try {
    res.json(await allMovimientos.removeMovimiento(req.params.id))
  } catch (err) {
    console.error(`Error while deleting programming language`, err.message)
    next(err)
  }
})

router.post("/", async function (req, res, next) {
  try {
    res.json(await allMovimientos.createMovimiento(req.body))
  } catch (err) {
    console.error(`Error while creating programming language`, err.message)
    next(err)
  }
})

/* PUT programming language */
router.put("/:id", async function (req, res, next) {
  try {
    res.json(await allMovimientos.updateMovimiento(req.params.id, req.body))
  } catch (err) {
    console.error(`Error while updating programming language`, err.message)
    next(err)
  }
})

router.post("/searchMov", async function (req, res, next) {
  try {
    res.json(
      await allMovimientos.searchMovimientoFilter(req.query.page, req.body)
    )
  } catch (err) {
    console.error(`Error while creating programming language`, err.message)
    next(err)
  }
})

module.exports = router
