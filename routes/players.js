const express = require("express")
const router = express()
const allPlayers = require("../services/allPlayers")
const parser = require("body-parser")
router.use(parser.json())

router.get("/:filter", async function (req, res, next) {
  try {
    console.log(req.body)
    res.json(await allPlayers.getPlayers(req.query.page, req.params.filter))
  } catch (err) {
    console.error(`Error while getting programming languages `, err.message)
    next(err)
  }
})

router.delete("/:id", async function (req, res, next) {
  try {
    res.json(await allPlayers.remove(req.params.id))
  } catch (err) {
    console.error(`Error while deleting programming language`, err.message)
    next(err)
  }
})

router.post("/", async function (req, res, next) {
  try {
    res.json(await allPlayers.create(req.body))
  } catch (err) {
    console.error(`Error while creating programming language`, err.message)
    next(err)
  }
})

/* PUT programming language */
router.put("/:id", async function (req, res, next) {
  console.log("body ", req.body)
  try {
    res.json(await allPlayers.update(req.params.id, req.body))
  } catch (err) {
    console.error(`Error while updating programming language`, err.message)
    next(err)
  }
})

module.exports = router
